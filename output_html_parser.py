__author__ = 'pphaneuf'


from bs4 import BeautifulSoup


MUTATION_POSITION_COLUMN_INDEX = 0


def get_header_row(sample_name_list):

    with open("output.html", "r") as output_html_file:

        soup = BeautifulSoup(output_html_file)

        for table in soup.findAll('table'):

            for row in table.findAll('tr'):

                columns = row.find_all('th')

                columns = [ele.text.strip() for ele in columns]

                if not is_list_or_cell_empty(columns):

                    intersection = set(sample_name_list).intersection(set(columns))

                    is_intersection_empty = not intersection

                    if not is_intersection_empty:

                        return str(row)

    return None


def get_mutation_row(mutation_position):

    with open("output.html", "r") as output_html_file:

        soup = BeautifulSoup(output_html_file)

        for table in soup.findAll('table'):

            for row in table.findAll('tr'):

                columns = row.find_all('td')

                columns = [ele.text.strip() for ele in columns]

                if not is_list_or_cell_empty(columns):

                    if columns[MUTATION_POSITION_COLUMN_INDEX] == mutation_position:

                        return str(row)

    return None


# TODO: Replace get_mutation_row with the below.
def get_mutation_rows_list(mutation_position, mutation_count):

    mutation_rows_list = []

    with open("output.html", "r") as output_html_file:

        soup = BeautifulSoup(output_html_file)

        for table in soup.findAll('table'):

            for row in table.findAll('tr'):

                columns = row.find_all('td')

                columns = [ele.text.strip() for ele in columns]

                if not is_list_or_cell_empty(columns):

                    if columns[MUTATION_POSITION_COLUMN_INDEX] == mutation_position:

                        mutation_rows_list.append(str(row))

                        if len(mutation_rows_list) == mutation_count:

                            return mutation_rows_list

    return mutation_rows_list


def is_list_or_cell_empty(input_list):
    return not input_list