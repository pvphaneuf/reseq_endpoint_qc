__author__ = 'pphaneuf'

import unittest

from reseq_endpoint_qc import skip_comment
from reseq_endpoint_qc import is_mutation_present
from reseq_endpoint_qc import get_mutation_type_lists
from reseq_endpoint_qc import get_mutation_position_occurrences
from reseq_endpoint_qc import is_multiple_mutation_at_same_position


class TestExecQc(unittest.TestCase):

    def test_is_line_comment_false(self):
        input_line = ("SNP"
                      "	1"
                      "	."
                      "	NC_007779_cfb"
                      "	167829"
                      "	A"
                      "	aa_new_seq=T"
                      "	aa_position=116"
                      "	aa_ref_seq=A"
                      "	codon_new_seq=ACA"
                      "	codon_number=116"
                      "	codon_position=1"
                      "	codon_ref_seq=GCA"
                      "	frequency_W3110-10-5-1_S1_filtered=0.1"
                      "	frequency_W3110-28-9-1_S4_filtered=0.3"
                      "	frequency_W3110-38-8-1_S7_filtered=0.7"
                      "	frequency_W3110_start=1	gene_list=fhuA"
                      "	gene_name=fhuA"
                      "	gene_position=346"
                      "	gene_product=ferrichrome outer membrane transporter	gene_strand=>"
                      "	html_gene_name=<i>fhuA</i>&nbsp;&rarr;"
                      "	locus_tag=Y75_p0147	snp_type=nonsynonymous	transl_table=11")

        output = skip_comment(input_line)

        self.assertFalse(output)

    def test_is_line_comment_true(self):
        input_comment_line = "#=GENOME_DIFF	1.0"

        output = skip_comment(input_comment_line)

        self.assertTrue(output)

    def test_is_mutation_present_true(self):
        mutation_included_in_sample = "frequency_W3110-10-5-1_S1_filtered=0.5"

        output = is_mutation_present(mutation_included_in_sample)

        self.assertTrue(output)

    def test_is_mutation_present_false(self):
        mutation_not_included_in_sample = "frequency_W3110_start=0"

        output = is_mutation_present(mutation_not_included_in_sample)

        self.assertFalse(output)

    def test_is_mutation_present_frequency_question_mark_false(self):
        mutation_not_included_in_sample = "frequency_W3110_start=?"

        output = is_mutation_present(mutation_not_included_in_sample)

        self.assertFalse(output)

    def test_is_mutation_present_frequency_delta_true(self):

        mutation_not_included_in_sample = "frequency_W3110_start=D"

        output = is_mutation_present(mutation_not_included_in_sample)

        self.assertTrue(output)

    def test_get_mutation_position_occurrences(self):

        input_file_name = "test_get_mutation_position_occurences.gd"

        mutation_positions_dict = get_mutation_position_occurrences(input_file_name)

        self.assertEquals(mutation_positions_dict["167829"], 1)
        self.assertEquals(mutation_positions_dict["2173361"], 2)
        self.assertEquals(len(mutation_positions_dict), 2)


    def test_is_multiple_mutation_at_same_position_false(self):

        mutation_line = ['DEL', '1', '67', 'NC_000913', '126351', '1', 'frequency=2.463355e-01', 'frequency_1-Glu_filtered=0', 'frequency_10-Glu_filtered=0', 'frequency_11-Glu_filtered=0', 'frequency_12-Glu_filtered=0', 'frequency_13-Glu-Redo_filtered=0', 'frequency_13-Glu_filtered=0', 'frequency_2-Glu_filtered=0', 'frequency_3-Glu_filtered=0', 'frequency_4-Glu_filtered=0', 'frequency_5-Glu_filtered=0', 'frequency_6-Glu_filtered=0', 'frequency_7-Glu_filtered=0', 'frequency_8-Glu_filtered=2.463355e-01', 'frequency_9-Glu_filtered=0', 'frequency_ssw_bop27_start_filtered=0', 'gene_list=aceF', 'gene_name=aceF', 'gene_position=coding (657/1893 nt)', 'gene_product=pyruvate dehydrogenase, dihydrolipoyltransacetylase component E2', 'gene_strand=>', 'html_gene_name=<i>aceF</i>&nbsp;&rarr;', 'locus_tag=b0115']

        mutation_positions_dict = {'126351': 1, '126352': 1, '126353': 1}

        output = is_multiple_mutation_at_same_position(mutation_line, mutation_positions_dict)

        self.assertFalse(output)


    def test_is_multiple_mutation_at_same_position_true(self):

        mutation_line = ['DEL', '1', '67', 'NC_000913', '126351', '1', 'frequency=2.463355e-01', 'frequency_1-Glu_filtered=0', 'frequency_10-Glu_filtered=0', 'frequency_11-Glu_filtered=0', 'frequency_12-Glu_filtered=0', 'frequency_13-Glu-Redo_filtered=0', 'frequency_13-Glu_filtered=0', 'frequency_2-Glu_filtered=0', 'frequency_3-Glu_filtered=0', 'frequency_4-Glu_filtered=0', 'frequency_5-Glu_filtered=0', 'frequency_6-Glu_filtered=0', 'frequency_7-Glu_filtered=0', 'frequency_8-Glu_filtered=2.463355e-01', 'frequency_9-Glu_filtered=0', 'frequency_ssw_bop27_start_filtered=0', 'gene_list=aceF', 'gene_name=aceF', 'gene_position=coding (657/1893 nt)', 'gene_product=pyruvate dehydrogenase, dihydrolipoyltransacetylase component E2', 'gene_strand=>', 'html_gene_name=<i>aceF</i>&nbsp;&rarr;', 'locus_tag=b0115']

        mutation_positions_dict = {'126351': 2, '126352': 1, '126353': 1}

        output = is_multiple_mutation_at_same_position(mutation_line, mutation_positions_dict)

        self.assertTrue(output)


    # TODO: Fix according to new output of

    # def test_get_mutation_matches_single_mutation_all(self):
    #     input_file_name = "test_output_single_mutation_all.gd"
    #
    #     expected_matching_mutation = {"SNP",
    #                                   "1",
    #                                   ".",
    #                                   "NC_007779_cfb",
    #                                   "167829",
    #                                   "A",
    #                                   "aa_new_seq=T",
    #                                   "aa_position=116",
    #                                   "aa_ref_seq=A",
    #                                   "codon_new_seq=ACA",
    #                                   "codon_number=116",
    #                                   "codon_position=1",
    #                                   "codon_ref_seq=GCA",
    #                                   "frequency_W3110-10-5-1_S1_filtered=1",
    #                                   "frequency_W3110-28-9-1_S4_filtered=1",
    #                                   "frequency_W3110-38-8-1_S7_filtered=1",
    #                                   "frequency_W3110_start=1",
    #                                   "gene_list=fhuA",
    #                                   "gene_name=fhuA",
    #                                   "gene_position=346",
    #                                   "gene_product=ferrichrome outer membrane transporter",
    #                                   "gene_strand=>",
    #                                   "html_gene_name=<i>fhuA</i>&nbsp;&rarr;",
    #                                   "locus_tag=Y75_p0147",
    #                                   "snp_type=nonsynonymous",
    #                                   "transl_table=11"}
    #
    #     start_sample_matching_mutations_list, start_sample_non_matching_mutations_list\
    #         = get_mutation_type_lists(input_file_name)
    #
    #     is_expected_matching_mutation_in_output = False
    #     for index in range(len(start_sample_matching_mutations_list)):
    #         if set(start_sample_matching_mutations_list[index]) == expected_matching_mutation:
    #             is_expected_matching_mutation_in_output = True
    #             break
    #
    #     self.assertTrue(is_expected_matching_mutation_in_output)
    #
    #     start_sample_non_matching_mutations_list_is_empty = not start_sample_non_matching_mutations_list
    #
    #     self.assertTrue(start_sample_non_matching_mutations_list_is_empty)
    #
    # # def test_are_starting_mutations_in_endpoints_multi_mutation_missing(self):
    # #     input_file_name = "test_output_mutli_mutation_missing.gd"
    # #
    # #     expected_matching_mutation = {"SNP",
    # #                                   "1",
    # #                                   ".",
    # #                                   "NC_007779_cfb",
    # #                                   "167829",
    # #                                   "A",
    # #                                   "aa_new_seq=T",
    # #                                   "aa_position=116",
    # #                                   "aa_ref_seq=A",
    # #                                   "codon_new_seq=ACA",
    # #                                   "codon_number=116",
    # #                                   "codon_position=1",
    # #                                   "codon_ref_seq=GCA",
    # #                                   "frequency_W3110-10-5-1_S1_filtered=1",
    # #                                   "frequency_W3110-28-9-1_S4_filtered=0.7",
    # #                                   "frequency_W3110-38-8-1_S7_filtered=1",
    # #                                   "frequency_W3110_start=1",
    # #                                   "gene_list=fhuA",
    # #                                   "gene_name=fhuA",
    # #                                   "gene_position=346",
    # #                                   "gene_product=ferrichrome outer membrane transporter",
    # #                                   "gene_strand=>",
    # #                                   "html_gene_name=<i>fhuA</i>&nbsp;&rarr;",
    # #                                   "locus_tag=Y75_p0147",
    # #                                   "snp_type=nonsynonymous",
    # #                                   "transl_table=11"}
    # #
    # #     expected_non_matching_mutation = {"SNP",
    # #                                       "15",
    # #                                       "486",
    # #                                       "NC_007779_cfb",
    # #                                       "285106",
    # #                                       "G",
    # #                                       "aa_new_seq=S",
    # #                                       "aa_position=163",
    # #                                       "aa_ref_seq=I",
    # #                                       "codon_new_seq=AGC",
    # #                                       "codon_number=163",
    # #                                       "codon_position=2",
    # #                                       "codon_ref_seq=ATC",
    # #                                       "frequency=0.0630",
    # #                                       "frequency_W3110-10-5-1_S1_filtered=1",
    # #                                       "frequency_W3110-28-9-1_S4_filtered=0.7",
    # #                                       "frequency_W3110-38-8-1_S7_filtered=0",
    # #                                       "frequency_W3110_start=1",
    # #                                       "gene_list=yagG",
    # #                                       "gene_name=yagG",
    # #                                       "gene_position=488",
    # #                                       "gene_product=sugar transporter",
    # #                                       "gene_strand=>",
    # #                                       "html_gene_name=<i>yagG</i>&nbsp;&rarr;",
    # #                                       "locus_tag=Y75_p0262",
    # #                                       "snp_type=nonsynonymous",
    # #                                       "transl_table=11"}
    # #
    # #     start_sample_matching_mutations_list, start_sample_non_matching_mutations_list \
    # #         = get_mutation_type_lists(input_file_name)
    # #
    # #     is_expected_matching_mutation_in_output = False
    # #     for index in range(len(start_sample_matching_mutations_list)):
    # #         if set(start_sample_matching_mutations_list[index]) == expected_matching_mutation:
    # #             is_expected_matching_mutation_in_output = True
    # #             break
    # #
    # #     self.assertTrue(is_expected_matching_mutation_in_output)
    # #
    # #     is_expected_non_matching_mutation_in_output = False
    # #     for index in range(len(start_sample_non_matching_mutations_list)):
    # #         if set(start_sample_non_matching_mutations_list[index]) == expected_non_matching_mutation:
    # #             is_expected_non_matching_mutation_in_output = True
    # #             break
    # #
    # #     self.assertTrue(is_expected_non_matching_mutation_in_output)
    # #
    # # # def test_are_starting_mutations_in_endpoints_single_mutation_missing(self):
    # # #
    # # # input_file_name = "test_output_single_mutation_missing.gd"
    # # #
    # # # self.assertFalse(are_starting_mutations_in_endpoints(input_file_name))
    # # #
    # # # def test_are_starting_mutations_in_endpoints_single_mutation_no_start(self):
    # # #
    # # # input_file_name = "test_output_single_mutation_no_start.gd"
    # # #
    # # # self.assertTrue(are_starting_mutations_in_endpoints(input_file_name))
    # # #
    # # # def test_are_starting_mutations_in_endpoints_multi_mutation_all(self):
    # # #
    # # #     input_file_name = "test_output_mutli_mutation_all.gd"
    # # #
    # # #     self.assertTrue(are_starting_mutations_in_endpoints(input_file_name))
    # # #
    # #
    # # # def test_are_starting_mutations_in_endpoints_multi_mutation_no_start(self):
    # # #
    # # #     input_file_name = "test_output_mutli_mutation_no_start.gd"
    # # #
    # # #     self.assertTrue(are_starting_mutations_in_endpoints(input_file_name))
