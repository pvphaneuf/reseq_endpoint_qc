__author__ = 'pphaneuf'

'''
Compatible with Python 3. Will crash for Python 2.
'''

import report_generator

import csv

from collections import Counter


STARTING_SAMPLE_FILE_FLAG = "start"
FILTERED_ENDPOINT_SAMPLE_FILE_FLAG = "filtered"

GD_MUTATION_POSITION_INDEX = 4


# TODO: Mix the "Mutations that Share Starting Positions" table with the other regular tables by using
# the same method between single and multiple mutations per position to find the next mutation within
# the output.html. This would get ride of the need for the "Mutations that Share Starting Positions" table
def main():
    input_file_name = "output.gd"

    reseq_endpoint_qc(input_file_name)


def reseq_endpoint_qc(input_file_name):
    sample_name_list = get_sample_names_from_input_file(input_file_name)

    start_sample_matching_mutations_list,\
    start_sample_non_matching_mutations_list,\
    new_mutations_list,\
    mutations_position_dict\
        = get_mutation_type_lists(input_file_name)

    report_generator.write_results_to_html(sample_name_list,
                                           start_sample_matching_mutations_list,
                                           start_sample_non_matching_mutations_list,
                                           new_mutations_list,
                                           mutations_position_dict)


# Currently pulls sample names from GD file combining all samples.
# Should rather pull from each samples individual GD files since
# will also be pulling many other stats from unique sample GD files.rm
def get_sample_names_from_input_file(input_file_name):
    sample_name_list = []

    with open(input_file_name) as tab_separated_values:

        # Will exit after scanning first line containing sample names.
        for line in csv.reader(tab_separated_values, delimiter="\t"):

            if skip_comment(line):
                continue

            sample_name_list = get_sample_names_from_mutation_line(line)
            break

    return sample_name_list


def get_sample_names_from_mutation_line(mutation_line):

    sample_name_list = []

    for item in mutation_line:

        if STARTING_SAMPLE_FILE_FLAG in item:
            sample_name_list.append(get_sample_name(item))

        elif FILTERED_ENDPOINT_SAMPLE_FILE_FLAG in item:
            sample_name_list.append(get_sample_name(item))

    return sample_name_list


def get_sample_name(mutation_entry):

    sample_name_index = mutation_entry.find('=')

    sample_name = mutation_entry[:sample_name_index]

    sample_name = sample_name.replace("frequency_", "")

    return sample_name


def get_mutation_position_occurrences(input_file_name):

    mutation_positions_list = []

    with open(input_file_name) as tab_separated_values:

        for line in csv.reader(tab_separated_values, delimiter="\t"):

            if skip_comment(line):
                continue

            mutation_positions_list.append(line[GD_MUTATION_POSITION_INDEX])

    mutation_positions_dict = Counter(mutation_positions_list)

    return mutation_positions_dict


def is_multiple_mutation_at_same_position(mutation_line, mutation_positions_dict):

    multiple_mutation_at_same_position = False

    mutation_position = mutation_line[GD_MUTATION_POSITION_INDEX]

    if mutation_positions_dict[mutation_position] > 1:
        multiple_mutation_at_same_position = True

    return multiple_mutation_at_same_position


def get_mutation_type_lists(input_file_name):

    start_sample_matching_mutations_list = []
    start_sample_non_matching_mutations_list = []
    new_mutations_list = []

    mutations_position_dict = get_mutation_position_occurrences(input_file_name)

    with open(input_file_name) as tab_separated_values:

        for line in csv.reader(tab_separated_values, delimiter="\t"):

            if skip_comment(line):

                continue

            elif is_multiple_mutation_at_same_position(line, mutations_position_dict):

                continue

            elif is_mutation_in_start_sample(line):

                if is_mutation_in_all_samples(line):

                    start_sample_matching_mutations_list.append(line)

                else:

                    start_sample_non_matching_mutations_list.append(line)

            else:
                new_mutations_list.append(line)

    return start_sample_matching_mutations_list,\
           start_sample_non_matching_mutations_list,\
           new_mutations_list,\
           mutations_position_dict


def is_mutation_in_start_sample(mutation_line):
    mutation_is_in_start_sample = False

    for item in mutation_line:

        if STARTING_SAMPLE_FILE_FLAG in item:
            mutation_is_in_start_sample = is_mutation_present(item)

            # Once staring sample found, exit search
            break

    return mutation_is_in_start_sample


def is_mutation_in_all_samples(mutation_line):

    mutation_present_amongst_all_samples = True

    for item in mutation_line:

        # Quick fail
        if not mutation_present_amongst_all_samples:
            break

        if STARTING_SAMPLE_FILE_FLAG in item:
            mutation_present_amongst_all_samples = is_mutation_present(item)

        elif FILTERED_ENDPOINT_SAMPLE_FILE_FLAG in item:
            mutation_present_amongst_all_samples = is_mutation_present(item)

    return mutation_present_amongst_all_samples


def is_mutation_present(sample):

    mutation_is_present = False

    mutation_frequency_string = get_mutation_freq_str(sample)

    if mutation_frequency_string is not '?':

        if mutation_frequency_string == 'D':    # 'D' in output.gd translates to 'Δ' on output.html.

            mutation_is_present = True

        else:   # Expecting a string that can be converted into a number.

            mutation_frequency = float(mutation_frequency_string)

            if mutation_frequency > 0:
                mutation_is_present = True

    return mutation_is_present


def get_mutation_freq_str(sample):

    mutation_inclusion_status_index = sample.rfind('=') + 1
    mutation_frequency_string = sample[mutation_inclusion_status_index:]

    return mutation_frequency_string


def skip_comment(line):
    comment_line = False

    if line[0][0] is '#':
        comment_line = True

    return comment_line


if __name__ == "__main__":
    main()
