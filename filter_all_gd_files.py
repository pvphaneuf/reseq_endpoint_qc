__author__ = 'pphaneuf'


from os import listdir
from os.path import isfile, join, splitext
import subprocess


file_names = [f for f in listdir(".") if isfile(join(".", f))]

gd_file_names = []

for file_name in file_names:

    name, extension = splitext(file_name)

    if extension == ".gd":
        gd_file_names.append(file_name)

partial_cmd_str = 'gdtools REMOVE -c "frequency<=0.05" -o '

cmds_to_execute = []

for gd_file_name in gd_file_names:

    name, extension = splitext(gd_file_name)

    filtered_file_name = name + "_filtered" + extension

    cmds_to_execute.append(partial_cmd_str
                           + filtered_file_name
                           + " " + gd_file_name)

for cmd in cmds_to_execute:

    subprocess.call(cmd, shell=True)
