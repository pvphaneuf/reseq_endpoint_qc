__author__ = 'pphaneuf'


import output_html_parser


MUTATION_POSITION_INDEX = 4
MUTATION_INDEX = 3


def write_results_to_html(sample_name_list,
                          start_sample_matching_mutations_list,
                          start_sample_non_matching_mutations_list,
                          new_mutations_list,
                          mutations_position_dict):

    html_string = "<!DOCTYPE html>"
    html_string += "<html>"

    html_string += "<head>"
    html_string += "</head>"
    html_string += output_style()
    html_string += "<body>"

    html_string += output_sample_list(sample_name_list)

    html_string += '<p><font size="8" color="purple">Mutations that Share Starting Positions</font></p>'
    html_string += create_same_position_mutations_table(sample_name_list, mutations_position_dict)

    html_string += '<p><font size="8" color="green">Mutations Matching with Start Sample</font></p>'
    html_string += create_mutation_table(sample_name_list, start_sample_matching_mutations_list)

    html_string += '<p><font size="8" color="red">Mutations Not Matching with Start Sample</font></p>'
    html_string += create_mutation_table(sample_name_list, start_sample_non_matching_mutations_list)

    html_string += '<p><font size="8" color="orange">Mutations Not in Start Sample</font></p>'
    html_string += create_mutation_table(sample_name_list, new_mutations_list)

    html_string += "</html>"
    html_string += "</body>"

    create_report(html_string)


def output_style():

    html_string = '<style type = "text/css">'

    html_string += 'body {font-family: sans-serif; font-size: 11pt;}'
    html_string += 'th {background-color: rgb(0,0,0); color: rgb(255,255,255);}'
    html_string += 'table {background-color: rgb(1,0,0); color: rgb(0,0,0);}'
    html_string += 'tr {background-color: rgb(255,255,255);}'

    html_string += "</style>"

    return html_string


def output_sample_list(sample_name_list):

    html_string = ""

    html_string += '<p><b>Samples</b></p>'

    for sample in sample_name_list:
        html_string += '<p><td>' + sample + "</p>"

    return html_string


def create_report(html_string):

    with open("starting_mutation_qc_report.html", "w", encoding='UTF-16') as html_file:
        html_file.write(html_string)


def create_same_position_mutations_table(sample_name_list, mutations_position_dict):

    html_string = ""

    html_string += '<table border="0" cellspacing="1" cellpadding="3">'

    html_string += output_html_parser.get_header_row(sample_name_list)

    for key in mutations_position_dict:

        mutation_count = mutations_position_dict[key]

        if mutation_count > 1:

            position_string_with_commas = get_mutation_position_with_commas(key)

            mutation_row_output_list = output_html_parser.get_mutation_rows_list(position_string_with_commas,
                                                                                 mutation_count)

            for line in mutation_row_output_list:
                if line is not None:
                    html_string += line

    html_string += '</table>'

    return html_string


def create_mutation_table(sample_name_list, mutations_list):

    html_string = ""

    html_string += '<table border="0" cellspacing="1" cellpadding="3">'

    html_string += output_html_parser.get_header_row(sample_name_list)

    for line in mutations_list:

        mutation_position_str = line[MUTATION_POSITION_INDEX]

        position_string_with_commas = get_mutation_position_with_commas(mutation_position_str)

        mutation_row_output = output_html_parser.get_mutation_row(position_string_with_commas)

        if mutation_row_output is not None:
            html_string += mutation_row_output

    html_string += '</table>'

    return html_string


def get_mutation_position_with_commas(mutation_position_string):

    format_input_string = "{:"
    format_input_string += str(len(mutation_position_string))
    format_input_string += ",d}"

    mutation_position_string_with_commas = format_input_string.format(int(mutation_position_string))

    return mutation_position_string_with_commas
