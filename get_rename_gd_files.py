import os
import shutil

local_sample_dirs = [name for name in os.listdir(".") if os.path.isdir(name)]

for dir in local_sample_dirs:

    file_to_copy_str = str(dir) + "/output/output.gd"
    destination_str = str(dir) + ".gd"

    # TODO: catch exception of not having /output within dir,
    # such as with the fastqs/ dirs.
    shutil.copyfile(file_to_copy_str, destination_str)
